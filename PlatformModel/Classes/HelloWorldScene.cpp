/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
	auto scene = HelloWorld::create();
	return scene;
}


// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
	this->initWithPhysics();
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	getPhysicsWorld()->setGravity(Vec2(0, 0.0));
	getPhysicsWorld()->setDebugDrawMask(0xffff);

	map = Sprite::create("./images/map_modified.png");
	addChild(map);

	car_node = Node::create();

	body_sprite = Sprite::create("./images/wood.png");
	body_sprite->setContentSize(Size(100, 50));
	
	body_body = PhysicsBody::createBox(Size(100, 50), PhysicsMaterial(0.1, 1.0, 0.0));
	body_body->setAngularDamping(0.5);
	body_body->setLinearDamping(0.5);
	body_body->setDynamic(true);
	body_body->setMass(1.0);

	listener = EventListenerKeyboard::create();
	
	//body_body->applyImpulse()
	//car_node->setPhysicsBody(body_body);

	car_node->addComponent(body_body);
	

	car_node->addChild(body_sprite);

	this->addChild(car_node);
	car_node->setPosition(Vec2(visibleSize.width / 2.0, visibleSize.height / 2.0));
	map->setPosition(Vec2(visibleSize.width / 2.0, visibleSize.height / 2.0));
	map->setScale(2.0);
	start_position = Vec2(visibleSize.width / 2.0, visibleSize.height / 2.0);
	listener->onKeyPressed = CC_CALLBACK_2(HelloWorld::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(HelloWorld::onKeyReleased, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    

	scheduleUpdate();
    return true;
}

void HelloWorld::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_W) {
		Vec2 pos = body_body->getPosition();
		const double force = 5000.0;
		Vec2 force_vec = Vec2(-1.0, 0);
		force_vec.rotateByAngle(force_vec, body_body->getRotation());
		force_vec.normalize();

		body_body->applyForce(force_vec * force);
	}
	if (keyCode == EventKeyboard::KeyCode::KEY_S) {
		
	}
	if (keyCode == EventKeyboard::KeyCode::KEY_A) {
		float torque = 15000000.0;
		body_body->applyTorque(torque);
	}
	if (keyCode == EventKeyboard::KeyCode::KEY_D) {
		float torque = 15000000.0;
		body_body->applyTorque(-torque);
	}
	log("Key with keycode %d pressed", keyCode);
}

void HelloWorld::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	log("Key with keycode %d released", keyCode);
}

void HelloWorld::update(float delta)
{
	Scene::update(delta);
	Vec2 offset = Vec2(body_body->getPosition());
	getDefaultCamera()->setPosition(offset);

}
